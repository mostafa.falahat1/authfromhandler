﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Authen2.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Authen2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly ICustomAuthenticationManager customAuthenticationManager;
        public UserController(ICustomAuthenticationManager customAuthenticationManager)
        {
            this.customAuthenticationManager = customAuthenticationManager;
        }

        [HttpPost("Authenticate")]
        public IActionResult Authenticate([FromBody] UserCred userCred)
        {
            var token = customAuthenticationManager.Authenticate(userCred.Username, userCred.Password);

            if (token == null)
                return Unauthorized();

            return Ok(token);
        }
        [HttpPost("Test")]
        [Authorize(Policy = "EmployeeWithMoreThan20Years")]
        public IActionResult Test()
        {
            return Ok("cbvxc");
        }
    }
}
