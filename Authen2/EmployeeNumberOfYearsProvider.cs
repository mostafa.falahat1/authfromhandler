﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Authen2
{
    public class EmployeeNumberOfYearsProvider : IEmployeeNumberOfYearsProvider
    {
        public int Get(string value)
        {
            if (value == "test2")
            {
                return 21;
            }
            return 10;
        }
    }
}
