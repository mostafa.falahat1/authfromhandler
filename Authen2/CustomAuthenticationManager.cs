﻿using Authen2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Authen2
{
  
        public interface ICustomAuthenticationManager
        {
        string Authenticate(string username, string password);

        IDictionary<string, Tuple<string, string>> Tokens { get; }
    }

        public class CustomAuthenticationManager : ICustomAuthenticationManager
        {

        private readonly IList<User> users = new List<User>
        {
            new User { Username= "test1", Password= "password1", Role = "Administrator" },
            new User { Username = "test2",Password= "password2", Role = "User" }
        };
        private readonly IDictionary<string, Tuple<string, string>> tokens =
    new Dictionary<string, Tuple<string, string>>();

        public IDictionary<string, Tuple<string, string>> Tokens => tokens;

      //  private readonly IDictionary<string, string> tokens = new Dictionary<string, string>();

         //   public IDictionary<string, string> Tokens => tokens;

            public string Authenticate(string username, string password)
            {
            //var users = new List<User>()
            //{
            //new User(){ Username="ali" , Password="11" },
            // new User(){ Username="hasan" , Password="22" }
            //};
            if (!users.Any(u => u.Username == username && u.Password == password))
            {
                return null;
            }
            //for (int i=0; i< users.Count; i++)
            //{
            //    if ( users[i].Username== username && users[i].Password == password)
            //    {
            //        Authentication = true;
            //    }
            //}

            var token = Guid.NewGuid().ToString();

            tokens.Add(token, new Tuple<string, string>(username,
            users.First(u => u.Username == username && u.Password == password).Role));

            return token;
            }
        }
    }

